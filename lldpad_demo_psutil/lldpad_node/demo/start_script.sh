#!/bin/bash
lldpad -V 6 -d
lldptool -L -i eth0 adminStatus=rx
cd /demo
python qwilfish/extras/probes/psutil_probe.py lldpad 10.113.0.2 30052 > /dev/null &

wait -n
exit $?
