#! /bin/bash
while true
do
    docker container prune -f
    docker-compose up --abort-on-container-exit --build
    sleep 5
done
