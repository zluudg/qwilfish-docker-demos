#!/bin/bash
mkfifo /demo/hook_fifo
LD_PRELOAD=/demo/libhook/libhook.so lldpd -d&
cd /demo
python qwilfish/extras/probes/malloc_hook_probe.py /demo/hook_fifo 10.211.0.2 > /dev/null &

wait -n
exit $?
