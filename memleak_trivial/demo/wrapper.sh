#!/bin/bash

unique_id=$(head -c8 < /dev/urandom| xxd -p -u)
python3 qwilfish/extras/probes/memleak_trivial_probe.py /demo/file_courier.txt > /dev/null &
qwilfish -o /demo/shared/${unique_id}.db &

wait -n
exit $?
