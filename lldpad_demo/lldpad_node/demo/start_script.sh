#!/bin/bash
mkfifo /demo/hook_fifo
LD_PRELOAD=/demo/libhook/libhook.so lldpad -V 6 -d
lldptool -L -i eth0 adminStatus=rx
cd /demo
python qwilfish/extras/probes/malloc_hook_probe.py /demo/hook_fifo 10.111.0.2 > /dev/null &

wait -n
exit $?
