#!/bin/bash

sleep 10

unique_id=$(head -c8 < /dev/urandom| xxd -p -u)

qwilfish -o /demo/shared/${unique_id}.db &

wait -n
exit $?
