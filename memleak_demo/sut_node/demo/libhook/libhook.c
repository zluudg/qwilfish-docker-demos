#include <stdio.h>
#include <unistd.h>
#include <dlfcn.h>
#include <fcntl.h>

#define FIFO "/demo/hook_fifo"

static __thread int no_hook;

void* malloc(size_t size)
{
    void* (*normal_malloc)(size_t size);
    void *ret;
    int fd;

    normal_malloc = dlsym(RTLD_NEXT, "malloc");

    if (no_hook)
        return normal_malloc(size);

    no_hook = 1;
    ret = normal_malloc(size);

    fd = open(FIFO, O_WRONLY | O_NONBLOCK);
    write(fd, "HOOK: Malloc'd\n", 15);
    close(fd);

    no_hook = 0;

    return ret;
}

void free(void *ptr)
{
    void (*normal_free)(void *ptr);
    int fd;

    normal_free = dlsym(RTLD_NEXT, "free");

    normal_free(ptr);

    fd = open(FIFO, O_WRONLY | O_NONBLOCK);
    write(fd, "HOOK: Free'd\n", 13);
    close(fd);

    return;
}
