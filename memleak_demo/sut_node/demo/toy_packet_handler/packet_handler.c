#include <stdio.h>
#include <pcap/pcap.h>
#include <signal.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <limits.h>

#define TIMEOUT_MS 100 // Timeout if no packets are seen
#define PROMISC 1
#define FILTER_EXP "ether proto 0x88cc" // LLDP packets
#define MAC_ADDR_LEN 6 // A MAC address is six bytes
#define MAC_ADDR_STR_LEN MAC_ADDR_LEN*3 // 2 chars per byte + separators + '\0'
#define ETHERTYPE_LEN 2 // Ethertype is always 2 bytes

#define LLDP_TYPE_END       0
#define LLDP_TYPE_CHASSISID 1
#define LLDP_TYPE_PORTID    2
#define LLDP_TYPE_TTL       3
#define LLDP_TYPE_PORTDESCR 4
#define LLDP_TYPE_SYSNAME   5
#define LLDP_TYPE_SYSDESCR  6
#define LLDP_TYPE_SYSCAP    7
#define LLDP_TYPE_MGMTADDR  8
#define LLDP_TYPE_ORGSPEC 127

typedef struct {
    u_char src[MAC_ADDR_LEN];
    u_char dst[MAC_ADDR_LEN];
    u_short ethertype;
} ethernet_header;

typedef struct {
    u_short type_length; // Type and length, use macros below
#define LLDP_TYPE(tl) ((tl & 0xFE00) >> 9)
#define LLDP_LEN(tl)   (tl & 0x01FF)
    u_char value_start_byte; // First byte of value
} lldp_tlv;


void signal_handler(int sig)
{
    switch (sig) {
        case SIGTERM:
            exit(0);
        default:
            exit(-1);
    }
}


static void handle_lldp(const u_char *packet)
{
    lldp_tlv *tlv;
    u_short type, length;
    u_char *vstart; // Temporarily store pointer to current TLV value start
    u_char *tmp_p;
    int has_portdescr = 0;
    int has_sysname   = 0;
    int has_sysdescr  = 0;

    tlv = (lldp_tlv*)(packet);


    do
    {
        type = LLDP_TYPE(htons(tlv->type_length));
        length = LLDP_LEN(htons(tlv->type_length));
        tmp_p = malloc(length);
        memcpy(tmp_p, tlv, length); // copy TLV to heap

        switch (type)
        {
            case LLDP_TYPE_END:
                break;
            case LLDP_TYPE_CHASSISID:
                break;
            case LLDP_TYPE_PORTID:
                break;
            case LLDP_TYPE_TTL:
                break;
            case LLDP_TYPE_PORTDESCR:
                if (!has_portdescr)
                {
                    has_portdescr = 1;
                }
                else
                {
                    return; // leak memory, no free
                }
                break;
            case LLDP_TYPE_SYSNAME:
                if (!has_sysname)
                {
                    has_sysname = 1;
                }
                break;
            case LLDP_TYPE_SYSDESCR:
                if (!has_sysdescr)
                {
                    has_sysdescr = 1;
                }
                break;
            case LLDP_TYPE_SYSCAP:
                break;
            case LLDP_TYPE_MGMTADDR:
                break;
            case LLDP_TYPE_ORGSPEC:
                break;
            default: // Reserved type, 9-126
                break;
        }

        tlv = (lldp_tlv*)((u_char*)(tlv) + 2 + length); // Jump to next tlv

        free(tmp_p); // free malloc'd pointer

    } while (LLDP_TYPE_END != type);
}


void lldp_handler(u_char *args,
                  const struct pcap_pkthdr *header,
                  const u_char *packet)
{
    const ethernet_header *eh;
    const u_char *et_p; // Pointer to start of ethertype of the current tag 
    u_short et;

    eh = (ethernet_header*)(packet);
    et_p = (u_char*)(&eh->ethertype); // Pointer to ethertype field
    et = ntohs(*(u_short*)(et_p));

    switch (et)
    {
        case 0x88cc:
            handle_lldp(et_p + sizeof(u_short)); // payload follows ethertype
            break;
        default:
            printf("Unrecognized ethertype: 0x%04x\n", et);
            break;
    }
}



int main(int argc, char *argv[])
{
    char *dev = argv[1]; // CLI argument (hopefully a valid interface)
    char errbuf[PCAP_ERRBUF_SIZE]; // Error string
    pcap_t *handle; // Session handle
    struct bpf_program fp; // The compiled filter

    // Register signal handler
    signal(SIGTERM, signal_handler);

    // Start a session (open the device)
    handle = pcap_open_live(dev, BUFSIZ, PROMISC, TIMEOUT_MS, errbuf);
    if (NULL == handle)
    {
        printf("Couldn't open device %s: %s\n", dev, errbuf);
        return(2);
    }
    // Make sure that it is an Ethernet device
    if (pcap_datalink(handle) != DLT_EN10MB)
    {
        printf("Device %s doesn't provide Ethernet headers!\n", dev);
        return(2);
    }
    // Compile and install the filter
    if (pcap_compile(handle, &fp, FILTER_EXP, 0, PCAP_NETMASK_UNKNOWN) == -1)
    {
        printf("Couldn't compile filter %s: %s\n",
               FILTER_EXP,
               pcap_geterr(handle));
        return(2);
    }
    if (pcap_setfilter(handle, &fp) == -1)
    {
        printf("Couldn't install filter %s: %s\n",
               FILTER_EXP,
               pcap_geterr(handle));
        return(2);
    }

    // Capture a packet!
    printf("Entering pcap_loop...\n");
    (void) pcap_loop(handle, -1, lldp_handler, NULL);

    // Close session
    pcap_close(handle);

    return(0);
}
