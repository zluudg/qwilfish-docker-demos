#!/bin/bash
cd toy_packet_handler/
mkfifo /demo/hook_fifo
LD_PRELOAD=/demo/libhook/libhook.so ./parent_proc eth0 &
cd /demo
python qwilfish/extras/probes/malloc_hook_probe.py /demo/hook_fifo 10.111.0.2 > /dev/null &

wait -n
exit $?
