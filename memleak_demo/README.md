## Docker networking and LLDP
To get to containers to talk ''raw ethernet'' with eachother, start by
creating a docker network:

docker network create --driver bridge my-network

Observe its NETWORK ID from ''docker network ls''. Using ''ip l'', should show
that a new bridge interface has been create with a name similar to the
NETWORK ID of the docker network. This is the bridge interface to which the
containers will be attached. By default, linux bridges do not forward certain
types of multicast frames such as LLDP. This behavior can be changed by
fiddling with a paramater in the kernel:

echo 16384 > /sys/class/net/<bridge name>/bridge/group_fwd_mask

Run this as a root user (''sudo su'') and after that, lldpd frames should be
forwarded between the containers as if they where connected by wire.
